package leelawattananonkul.arisara.lab11;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import leelawattananonkul.arisara.lab5.Athlete;
import leelawattananonkul.arisara.lab5.Gender;

public class AthleteFormV8 extends AthleteFormV7 implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenu dataMenu;
	protected JMenuItem displayItem, sortItem, searchItem, removeItem;
	protected Athlete athlete;
	protected String nameObject, nationObject, gender, birthObject, display;
	protected Gender genderObject;
	protected double weightObject, heightObject;
	protected ArrayList<Athlete> athletes = new ArrayList<>();
	protected ArrayList<String> typeList = new ArrayList<>();
	protected ArrayList<String> competitionText = new ArrayList<>();
	
	public AthleteFormV8(String title) {
		super(title);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV8 AthleteForm8 = new AthleteFormV8("Athlete Form V8");
		AthleteForm8.addComponents();
		AthleteForm8.setFrameFeatures();
	}
	
	public void setFrameFeatures() {
		super.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	protected void addListeners() {
		super.addListeners();
		
		dataMenu = new JMenu("Data");
		displayItem = new JMenuItem("Display");
		sortItem = new JMenuItem("Sort");
		searchItem = new JMenuItem("Search");
		removeItem = new JMenuItem("Remove");
		
		displayItem.addActionListener(this);
		sortItem.addActionListener(this);
	}
	
	public void addComponents() {
		super.addComponents();
		dataMenu.add(displayItem);
		dataMenu.add(sortItem);
		dataMenu.add(searchItem);
		dataMenu.add(removeItem);
		menuBar.add(dataMenu);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		Object cmd = event.getSource();
		if (cmd == okButt) {
			addAthlete();
		} if (cmd == displayItem) {
			displayAthletes();
		} if (cmd == sortItem) {
			Collections.sort(athletes, new HeightComparator());
			displayAthletes();
		}
		super.actionPerformed(event); 
	}
	
	protected void handleOKButton() {
		message = "";
		for (int i = 0; i < athletes.size(); i++) {
			message += "Name = " + athletes.get(i).getName() 
					+ ", Birthdate = " + athletes.get(i).getBirthdate() 
					+ ", Weight = " + athletes.get(i).getWeight()
					+ ", Height = " + athletes.get(i).getHeight()
					+ ", Nation = " + athletes.get(i).getNationality()
					+ "\nGender = " + athletes.get(i).getGender()
					+ "\nAddress = " + competitionText.get(i)
					+ "\nType = " + typeList.get(i) + " \n";
		}
		
		JOptionPane.showMessageDialog(null, message); 		
	}
	
	private void addAthlete() {
		try {
			objectAthlete();
		} catch (Exception e) {
			System.out.println("Please fill your information properly");
		}
	}
	
	private void objectAthlete() {
		nameObject = nameText.getText();
		weightObject = Double.parseDouble(weightText.getText()); 
		heightObject = Double.parseDouble(heightText.getText()); 
		nationObject = nationText.getText();
		gender = buttonGroup.getSelection().getActionCommand();
		genderObject = athleteGender();
		birthObject = birthText.getText();
		
		Athlete athletic = new Athlete(nameObject, weightObject, heightObject, genderObject, nationObject, birthObject);
		athletes.add(athletic);
		typeList.add(typeCombo.getSelectedItem().toString()); 
		competitionText.add(competText.getText());
	}
	
	private Gender athleteGender() {
		if (maleButton.isSelected()) {
			return Gender.MALE;
		}
		else if (femaleButton.isSelected()) {
			return Gender.FEMALE;
		}
		return genderObject;
	}
	
	private void displayAthletes() {
		display = "";
		for (int i = 0; i < athletes.size(); i++) {
			display += (i+1) + ". " + athletes.get(i) + "\n";
		}
		JOptionPane.showMessageDialog(null, display);
	}
	
	class HeightComparator implements Comparator<Athlete> {
		@Override
		public int compare(Athlete athlete1, Athlete athlete2) {
			double height1 = athlete1.getHeight();
			double height2 = athlete2.getHeight();
			if (height1 > height2) {
				return 1;
			} else if (height1 < height2) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}
