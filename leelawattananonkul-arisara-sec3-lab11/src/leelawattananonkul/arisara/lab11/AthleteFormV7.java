package leelawattananonkul.arisara.lab11;

/*
 * @author  Arisara Leelawattananonkul
 * no. 593040687-9
 * @since 2018-05-01
*/

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import leelawattananonkul.arisara.lab9.AthleteFormV6;

public class AthleteFormV7 extends AthleteFormV6 implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected JColorChooser chooser;
	protected JPanel colorPanel;
	protected JFileChooser openFile, saveFile;
	protected Color color;
	
	public AthleteFormV7(String title) {
		super(title);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV7 AthleteForm7 = new AthleteFormV7("Athlete Form V7");
		AthleteForm7.addComponents();
		AthleteForm7.setFrameFeatures();
	}
	
	public void setFrameFeatures() {
		super.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public void addComponents() {
		super.addComponents();
		
		fileMenu.setMnemonic(KeyEvent.VK_F);
		addMenuItem(newItem, KeyEvent.VK_N, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		addMenuItem(openItem, KeyEvent.VK_O, KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		addMenuItem(saveItem, KeyEvent.VK_S, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		addMenuItem(exitItem, KeyEvent.VK_X, KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		
		newItem.setActionCommand("newItem");
		openItem.setActionCommand("openItem");
		saveItem.setActionCommand("saveItem");
		exitItem.setActionCommand("exitItem");
		customColor.setActionCommand("customColor");
		
		this.setContentPane(v6Panel);
	}
	
	public void addMenuItem(JMenuItem newItem, int NKey, KeyStroke AKey) {
		newItem.addActionListener(this);
		newItem.setMnemonic(NKey);
		newItem.setAccelerator(AKey);
	}
	
	protected void addListeners() {
		super.addListeners();
		fileMenu.addActionListener(this);
		customColor.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if ("openItem".equals(event.getActionCommand())) {
			openFile = new JFileChooser();
			openFile.setDialogType(JFileChooser.OPEN_DIALOG); 
			int result = openFile.showOpenDialog(null); 
			if (result == JFileChooser.APPROVE_OPTION) { 
				File selected = openFile.getSelectedFile();
				JOptionPane.showMessageDialog(this, "Opening : " + selected.getName() + ".");
			} else { 
				JOptionPane.showMessageDialog(this, "Opening command cancelled by user.");
			}
			
		} if ("saveItem".equals(event.getActionCommand())) {
			saveFile = new JFileChooser();
			saveFile.setDialogType(JFileChooser.SAVE_DIALOG); 
			int result = saveFile.showSaveDialog(null); 
			if (result == JFileChooser.APPROVE_OPTION) { 
				File selected = saveFile.getSelectedFile();
				JOptionPane.showMessageDialog(this, "Saving : " + selected.getName() + "");
			} else { 
				JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
			}
			
		} if ("exitItem".equals(event.getActionCommand())) {
			System.exit(0); 
		
		} if ("customColor".equals(event.getActionCommand())) {
			Color color = JColorChooser.showDialog(null, "Choose color", Color.WHITE); 
			competText.setBackground(color);	
		}		
		
	}

}
